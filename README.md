# Front End
[![N|Solid](https://plus.google.com/u/1/photos/photo/103601197889561599750/6615165745746840626?authkey=CMPl2urRy9y4kAE)
Para executar -> ng serve
* http://localhost:4200/tasks

# Web Service glassfish na linguagem 
O Projeto foi feito com NetBeans no S.O Ubuntu.
Ip: http://localhost:8080/WebService/webresources

* End points:
	* obter todas as Tasks --> get: http://localhost:8080/WebService/webresources/tasks
	* obter task por id --> get: http://localhost:8080/WebService/webresources/tasks/{id}
	* criar task --> post: http://localhost:8080/WebService/webresources/tasks
	* atualizar taks -->  put: http://localhost:8080/WebService/webresources/tasks
	* remover taks --> delete: http://localhost:8080/WebService/webresources/tasks/{id}

## Banco de dados
É Necessário ter um banco de dados Mysql
* DATABASE 
	* Ip: http:localhost:DBTask
	
[![N|Solid](https://plus.google.com/u/1/photos/photo/103601197889561599750/6615165257973740594)



