import { TasklistComponent } from './tasklist/tasklist.component';
import { Routes, RouterModule } from '../../node_modules/@angular/router';
import { NgModule } from '../../node_modules/@angular/core';

const appRoutes: Routes = [
    {path: '', component: TasklistComponent}
];

@NgModule({
    imports:[RouterModule.forRoot(appRoutes)],
    exports:[RouterModule]
})
export class AppRoutingModule {}
