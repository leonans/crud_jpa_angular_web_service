import { Component, OnInit } from '@angular/core';
import { TaskService } from './task.service';
import { Router } from '../../../node_modules/@angular/router';
import { Observable } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {

  private tasks: any = [];

  constructor(private taskService: TaskService,
       private router: Router,
  ) {


  }

  ngOnInit() {
    this.update();
    this.taskService.emitirTaskAtualizada.subscribe(
      (task:any) =>{
        this.update();
      }
    );

    this.taskService.emitirTaskDeletada.subscribe(
      (task:any) =>{
        this.update();
      }
    );

  }

  update() {
    this.taskService.getTasks().subscribe(
      (params: any) => {
        this.tasks = params;
      }
    );

  }


  criarTask(){
    this.router.navigate(['/tasks',0,'editar']);
  }

}
