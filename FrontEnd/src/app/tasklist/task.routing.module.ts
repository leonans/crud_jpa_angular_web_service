import { TaskFormComponent } from './task-form/task-form.component';
import { NgModule} from '@angular/core';
import { TasklistComponent } from './tasklist.component';
import { TaskDetalheComponent } from './task-detalhe/task-detalhe.component';
import { Routes, RouterModule } from '../../../node_modules/@angular/router';

const taskRoutes: Routes  = [
    {path: 'tasks', component: TasklistComponent, children :[
        {path: ':id', component: TaskDetalheComponent},
        {path: ':id/editar', component: TaskFormComponent},
    ]
    }   
];

@NgModule({
    imports:[RouterModule.forChild(taskRoutes)],
    exports:[RouterModule]
})

export class TaskRoutingModule{}