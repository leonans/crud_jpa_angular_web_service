import { Injectable, EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private tasks: any =[];
  private apiUrl: string = '';
  //[ {id:1, titulo: 'Task 01', status: 'executando'},
  //   {id:2, titulo: 'Task 02', status: 'parado'},
  //   {id:3, titulo: 'Task 03', status: 'criado'}
  // ];
  public emitirTaskAtualizada = new EventEmitter<string>();
  public emitirTaskDeletada = new EventEmitter<string>();



  ngOnInit() {
    this.apiUrl = environment.apiUrl;
  }
  constructor(private httpClient:HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  getTasks() {
    let url = this.apiUrl+"/tasks";
    return this.httpClient.get(url);
  }

  getTask(id:number) {
    let url = this.apiUrl+"/tasks/"+id;
    return this.httpClient.get(url);
  }

  removeTask(id:number) {
    let url = this.apiUrl+"/tasks/";

    this.httpClient.delete(url+id)
     .subscribe(
       res => {
         this.emitirTaskDeletada.emit(id);
       },
       err => {
         console.log("Error occured");
       }
     );

  }

  atualizaTask(task:any)  {
    let url = this.apiUrl+"/tasks";

     this.httpClient.put(url, task)
      .subscribe(
        res => {
          this.emitirTaskAtualizada.emit(task.id);
        },
        err => {
          console.log("Error occured");
        }
      );

  }

  addTask(task:any){
    task.id = Math.floor(Math.random() * 50);
    // tasks.push(task);
  }



}
