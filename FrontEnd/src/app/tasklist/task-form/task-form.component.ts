import { Component, OnInit } from '@angular/core';
import { Subscription } from '../../../../node_modules/rxjs';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

  id: number;
  inscricao: Subscription;
  task: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService) { }

    ngOnInit() {
      this.inscricao = this.route.params.subscribe(
        (params: any) => {
          this.id = params['id'];
         //obtendo tarefa
          this.taskService.getTask(this.id).subscribe(
            (params: any) => {
                this.task = params;
            }
          );
        }
      );

      this.taskService.emitirTaskAtualizada.subscribe(
        (task:any) =>{
          this.router.navigate(['/tasks']);
        }
      );

    }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  atualizarTask() {

    if(!this.task.id){
      this.taskService.addTask(this.task);
    }

    this.taskService.atualizaTask(this.task);

  }

}
