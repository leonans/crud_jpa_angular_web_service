import { TaskRoutingModule } from './task.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskDetalheComponent } from './task-detalhe/task-detalhe.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { FormsModule } from '@angular/forms';
import { TaskService } from './task.service';

@NgModule({
  imports: [
    CommonModule,
    TaskRoutingModule,
    FormsModule,
  ],
  declarations: [
    TaskDetalheComponent,
    TaskFormComponent
  ]
})
export class TasklistModule { }
