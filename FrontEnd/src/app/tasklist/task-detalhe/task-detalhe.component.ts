import { Component, OnInit } from '@angular/core';
import { Subscription } from '../../../../node_modules/rxjs';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { TaskService } from '../task.service';


@Component({
  selector: 'app-task-detalhe',
  templateUrl: './task-detalhe.component.html',
  styleUrls: ['./task-detalhe.component.css']
})
export class TaskDetalheComponent implements OnInit {

  id: number;
  inscricao: Subscription;
  task: any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService) { }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.id = params['id'];
       //obtendo tarefa
        this.taskService.getTask(this.id).subscribe(
          (params: any) => {
            this.task = params;
          }
        );

      }
    );

    this.taskService.emitirTaskDeletada.subscribe(
      (id:any) =>{
        this.router.navigate(['/tasks']);
      }
    );

  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  editarTask() {
    this.router.navigate(['/tasks',this.task.id,'editar']);
  }

  removerTask() {
    this.taskService.removeTask(this.id);
  }


}
