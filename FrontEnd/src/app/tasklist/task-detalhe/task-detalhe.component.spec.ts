import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDetalheComponent } from './task-detalhe.component';

describe('TaskDetalheComponent', () => {
  let component: TaskDetalheComponent;
  let fixture: ComponentFixture<TaskDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
