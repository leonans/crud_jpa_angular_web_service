import { TasklistModule } from './tasklist.module';

describe('TasklistModule', () => {
  let tasklistModule: TasklistModule;

  beforeEach(() => {
    tasklistModule = new TasklistModule();
  });

  it('should create an instance', () => {
    expect(tasklistModule).toBeTruthy();
  });
});
