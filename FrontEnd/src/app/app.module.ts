import { TasklistModule } from './tasklist/tasklist.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { TasklistComponent } from './tasklist/tasklist.component';
import { TaskService } from './tasklist/task.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    TasklistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TasklistModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
