var http = require("http");
var express = require('express');
var bodyParser = require('body-parser')

var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static(__dirname));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var tasks = [
		{"id":1, "titulo": "Task 01", "status": "executando"},
		{"id":2, "titulo": "Task 02", "status": "parado"},
		{"id":3, "titulo": "Task 03", "status": "criado"}
	]
	
app.get('/tasks', function (req, res) {
	console.log("Recebida");
		
	console.log(tasks);
	
	res.json(tasks);
	
});

app.get('/tasks/:id', function (req, res) {
	
	console.log("Recebida");
    const id = req.params.id;
	
	 for (let index = 0; index < tasks.length; index++) {
         let task = tasks[index];
         if(task.id == id) {
			 	console.log(task);

           res.json(task);
		   return;
         }
     }
	
	console.log(tasks);
	
	res.json(tasks);
	
});

app.post('/tasks', function (req, res) {
	
	console.log("Recebida post");

    const id = req.body.id;
	
	 for (let index = 0; index < tasks.length; index++) {
         let task = tasks[index];
         if(task.id == id) {
			console.log(task);
			task = req.body;
			console.log(task);

            res.json(task);
		   return;
         }
     }
	
	console.log(tasks);
	
	res.json();
	
});


var httpServer = http.createServer(app);
console.log('starting server on port 8088');

httpServer.listen('8088');

