/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.Task;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import util.JPAUtil;

/**
 *
 * @author root
 */
public class TaskDAO {
    
    EntityManager em;
     
    public TaskDAO () {
       em = new JPAUtil().getEntityManager();
    }
    
    public void Add(Task task){
     
        try {
            em.getTransaction().begin();
            em.persist(task);
            em.getTransaction().commit();
            em.close();

        } catch (Exception e) {
            throw new RuntimeException("Erro ao criar");
        }      
    }
    
     public void Update(Task task){
      
        try {
            em.getTransaction().begin();
            em.merge(task);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao atualizar");
        }      
    }
    
    public Task getById(int id){
        
        Task task = null;
        
        try {
            em.getTransaction().begin();
            task = em.find(Task.class, id);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            
            throw new RuntimeException("Erro ao obter por id");
        }  
        
        return task;
    }
    
    public void Remove(int id){
        
        try {
            em.getTransaction().begin();
            Task task = em.find(Task.class, id);
            em.remove(task);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            
            throw new RuntimeException("Erro ao remover");
        }  
    }
    
     public List<Object> getList(){
        
        List<Object> tasks = null;
        
         try {
                Query query = em.createNativeQuery("select * from TASK");
                tasks =  query.getResultList();
         } catch (Exception e) {
            throw new RuntimeException("Erro ao retornar lista");
         }
        
        return tasks;
    }
    
}
