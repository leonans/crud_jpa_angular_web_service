/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import bean.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.TaskDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;


/**
 * REST Web Service
 *
 * @author root
 */
@Path("tasks")
public class AppResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AppResource
     */
    public AppResource() {
    }

    /**
     * Retrieves representation of an instance of bean.AppResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
         
        TaskDAO dao = new TaskDAO();
        List<Task> tasks = new ArrayList<Task> ();
        
        List<Object> list = dao.getList();
        
         Object[] object= null;
        for (int i = 0; i < list.size(); i++) {
             object = (Object[]) list.get(i);
             
            Task task =  new Task();
            task.setId((Integer) object[0]);
            task.setTitulo((String) object[1]);
            task.setStatus((String) object[2]);
            task.setDescricao((String) object[3]);
            task.setDataCriacao((Date) object[4]);
            task.setDataEdicao((Date) object[5]);
            task.setDataRemocao((Date) object[6]);
            task.setDataConclusao((Date) object[7]);

            tasks.add(task);
        }

        String json = new Gson().toJson(tasks);
        

        return json;
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("id") int id){
         
        TaskDAO dao = new TaskDAO();
        Task task = null;
        Gson gson = new Gson();
        task = dao.getById(id);
        if(task == null) {
            task = new Task();
        }
            
        return gson.toJson(task);

    }
    

    /**
     * PUT method for updating or creating an instance of AppResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
        Task task = new Gson().fromJson(content, Task.class);
        TaskDAO dao = new TaskDAO();
        dao.Update(task);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postJson(String content) {
        
       Task task = new Gson().fromJson(content, Task.class);

        TaskDAO dao = new TaskDAO();
        dao.Add(task);
        
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteJson(@PathParam("id") int id){ 
         TaskDAO dao = new TaskDAO();
         dao.Remove(id);
    }
    
}
